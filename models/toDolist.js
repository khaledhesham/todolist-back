const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    description:{
        type:String,
    },
    status:{
        type:String
    },
    userId:{
        type:Number
    },
});

module.exports = mongoose.model('ToDoList', userSchema);
