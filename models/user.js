const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        unique:true
    }
},{_id:false});
userSchema.plugin(AutoIncrement,{inc_field: '_id'});
module.exports = mongoose.model('User', userSchema);
