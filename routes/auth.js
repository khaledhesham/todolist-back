const express = require('express');
const router = express.Router();
const auth = require("../controllers/auth");


router.post('/signUP', auth.signUP);
router.post('/logIn', auth.login);
module.exports = router;
