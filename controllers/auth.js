const User = require("../models/user");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const validator = require('validator');
const passwordValidator = require('password-validator');
let  schema = new passwordValidator();
schema
    .is().min(8)                                    // Minimum length 8
    .is().max(100)                                  // Maximum length 100
    .has().uppercase()                              // Must have uppercase letters
    .has().lowercase()                              // Must have lowercase letters
    .has().digits()                                 // Must have digits
    .has().not().spaces()                           // Should not have spaces
    .is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values
exports.signUP  =  async  (req,res,next)=>{
    let email = req.body.email;
    let password = req.body.password;
    try {
        let user =  await  User.findOne({ email:email});

        if (user) {
            res.status(400).json({message:"email is existed"});
        } else {
            if (!validator.isEmail(email)){
                res.status(400).json({message:"email is not valid"});
            }
            else {
                let passValidator = schema.validate(password, {list: true});
                if (passValidator.length !== 0) {
                    res.status(400).json({message: "password is not valid", error: passValidator});
                } else {
                    let pass = await bcrypt.hash(password, 12);
                    const  user = new User({
                        email: email,
                        password:pass
                    });
                    try {
                        let savedUser =  await  user.save();
                        res.status(201).json({
                            "id": savedUser.id,
                            "email":savedUser.email
                        });
                    }
                    catch (e) {
                        res.status(500).json({error: e});
                    }


                }
            }
        }
    }
    catch (e) {
        console.log(e);
        res.status(404).json({message:"can't connect to DB"});
    }

};
exports.login = async (req,res,next) => {
    let email = req.body.email;
    let password = req.body.password;
    let user =  await  User.findOne({ email:email});
    if (user) {
        let doMatch  = await  bcrypt.compare(password,user.password);
        if(doMatch){
            const token  = jwt.sign({userId : user.id , email:email},"somesupersecretsecret",{expiresIn: '1h'});
            res.status(200).json({token:token, expiresIn : 3600});
        }
        else {
            res.status(400).json({message: "Wrong Password." });
        }
    } else {
    res.status(400).json("A user with this email  could not be found.");
    }
};
