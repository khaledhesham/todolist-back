const User = require('../models/user');
const ToDoList = require("../models/toDolist");
exports.addToDo = async (req,res,next)=>{
    let email = req.body.email;
    let status = req.body.status;
    let description = req.body.description ;

    let user = await  User.findOne({email:email});
    const toDo  = new ToDoList({
       status:status,
       description:description,
       userId: user.id
    });
    let savedToDo =  await  toDo.save();
    res.status(201).json({
        "ToDO": {
            "id": savedToDo.id,
            "email": savedToDo.description,
            "status": savedToDo.status
        }
    });
};
exports.getToDo =async (req,res,next)=>{
    let email = req.body.email;
    let user = await  User.findOne({email:email});
   let toDoLists  =  await  ToDoList.find({userId:user.id});
   res.status(201).json({"ToDos":toDoLists});
};
