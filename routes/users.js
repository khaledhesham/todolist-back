const express = require('express');
const router = express.Router();
const isAuth = require('../middleWare/is-auth');
const user = require('../controllers/user');
router.post('/addToDo',isAuth,user.addToDo);
router.get('/getToDo',isAuth,user.getToDo);
module.exports = router;
